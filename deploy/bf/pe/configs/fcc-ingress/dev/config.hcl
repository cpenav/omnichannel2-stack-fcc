vault {
  address = "https://vault.fif.tech:8200"
  unwrap_token = false
  renew_token = true
  grace = "120h"

  ssl {
      enabled = true
      verify = true
  }
}

upcase = true
log_level = "err"

exec {
  command = "/bin/sh"
  env {
  pristine = true
  }
}

secret {
  format = "{{ key }}"
  no_prefix = true
  path = "kv/clusters/banco-pe-omni-dcc-test-cluster/shared/manual/fcc-ingress"
}